import os
import glob
import numpy as np
from app import app
import pandas as pd
import bisect as bs
import seaborn as sns
from flask import session
from datetime import datetime
import matplotlib.pyplot as plt
import sklearn.datasets as data
from app.controllers.scd import *
from werkzeug.utils import secure_filename


app.config["ALLOWED_FILE_EXTENSIONS"] = ["CSV", "TXT","DATA"]
app.config["MATRIZ_UPLOADS"] = "app/static/uploads"
app.config["ALLOWED_MODEL_EXTENSIONS"] = ["PCK"]


def multiplica_pesos(df, vetor_pesos, nameColumns):
    i=0
    vetor_pesos = list(map(int, vetor_pesos))
    for column in nameColumns:
        df[column] = df[column].apply(lambda x: x*vetor_pesos[i])
        i+=1
    return df


def mais_representativos(matriz, numGrupos):
    dt = pd.DataFrame(matriz)
    dt.rename(columns = {0: 'NomeArq'}, inplace=True)
    dt.rename(columns = {1: 'Grupo'}, inplace=True)
    dt.rename(columns = {2: 'ProbGrupo'}, inplace=True)
    dt.Grupo= dt.Grupo.astype(int)
    dt.ProbGrupo= dt.ProbGrupo.astype(float)

    representativos = []
    for i in range(numGrupos):
        daux = dt[dt['Grupo']==i]
        representativos.append(daux.loc[daux.ProbGrupo == 1, 'NomeArq'].values[0])

    return dt, representativos

def caracteristicas_grupos(da, numGrupos):
    da = da.drop('NomeArq', axis=1)
    nameColumns = ['ContPagPa', 'ContPagAt', 'ContPag', 'PropNum', 'ContCol', 'Desdob', 'AchouDRE', 'AchouRLP', 'AchouELP', 'PosAtPa1', 'PosAtPa2', 'PosAtPa3', 'PosAtPa4', 'TipoDoc1', 'TipoDoc2', 'Grupo']
    da = da[nameColumns]
    caracteristicasMedias = []
    caracteristicasMedianas = []
    for i in range(numGrupos):
        daux = da[da['Grupo']==i]
        auxMedias = []
        auxMedianas = []
        for column in nameColumns:
            if(column == 'ContPagPa' or column == 'ContPagAt' or column == 'ContPag' or column == 'PropNum' or column == 'ContCol'):
                auxMedias.append(float("{0:.2f}".format(daux[column].mean())))
                auxMedianas.append(float("{0:.2f}".format(daux[column].median())))
            else:
                auxMedias.append(str("{:.0%}".format(daux[column].mean())))
                auxMedianas.append(str("{:.0%}".format(daux[column].median())))

        caracteristicasMedias.append(auxMedias)
        caracteristicasMedianas.append(auxMedianas)
    return caracteristicasMedias, caracteristicasMedianas, nameColumns

def save_to_csv(df):
    destino = "app/static/downloads/docsclassificados.csv"
    df.to_csv(destino, sep='\t', index=False)


def salva_imagens(standard_data, std_data_labeled, clusterer, vetor_opcoes, da, cols, numGrupos):
    sns.set_context('poster')
    sns.set_style('white')
    sns.set_color_codes()
    plot_kwds = {'alpha' : 0.5, 's' : 80, 'linewidths':0}
    cols.remove('Grupo')

    if(vetor_opcoes[0]==True):
        get_mds(standard_data,'viridis','distribuicao')

    if(vetor_opcoes[1]==True):
        plt.figure(figsize=(12, 9), dpi=250)
        clusterer.minimum_spanning_tree_.plot(edge_cmap='viridis',
                                          edge_alpha=0.6,
                                          node_size=180,
                                          edge_linewidth=2)
        plt.savefig("app/static/img/analise/grafo_distancias.png",dpi=250,bbox_inches = 'tight',
        pad_inches = 0.2)
        plt.clf()

    if(vetor_opcoes[2]==True):
        plt.figure(figsize=(16, 10), dpi=250)
        clusterer.single_linkage_tree_.plot(cmap='viridis', colorbar=True)
        plt.savefig("app/static/img/analise/dendrograma.png",dpi=250,bbox_inches = 'tight',
        pad_inches = 0.2)
        plt.clf()

    if(vetor_opcoes[3]==True):
        plt.figure(figsize=(16, 10), dpi=250)
        clusterer.condensed_tree_.plot(select_clusters=True, selection_palette=sns.color_palette())
        plt.savefig("app/static/img/analise/cluster_tree.png",dpi=250,bbox_inches = 'tight',
        pad_inches = 0.2)
        plt.clf()

    if(vetor_opcoes[4]==True):
        plt.figure(figsize=(12, 9), dpi=250)
        get_mds(std_data_labeled, 'spring', 'classificado')

    if(vetor_opcoes[5]==True):
        for col in cols:
            dims = (16,10)
            fig, ax = plt.subplots(figsize=dims)
            sns.violinplot(x="Grupo", y=col, data=da, ax=ax)
            plt.title('Gráfico de Violino - '+col+' x Grupos')
            name = "app/static/img/analise/violino"+str(col)+".png"
            plt.savefig(name,dpi=250,bbox_inches = 'tight',
            pad_inches = 0.2)
            plt.clf()


def analise_grupo(df, clusterer, nameFiles):
    df['Grupo'] = clusterer.labels_
    df['NomeArq'] = nameFiles
    numGrupos = len(pd.Series(clusterer.labels_).value_counts())-1
    cols = list((df.loc[df['Grupo'] == 0]).columns)[0:16]
    matriz_concat = np.column_stack((nameFiles, clusterer.labels_, clusterer.probabilities_))
    da = df[df['Grupo']>=0]

    return numGrupos, cols, matriz_concat, da


def clean_files():
    files = glob.glob('app/static/uploads/*')
    for f in files:
        os.remove(f)

    files = glob.glob('app/static/img/analise/*')
    for f in files:
        os.remove(f)

    files = glob.glob('app/static/downloads/*')
    for f in files:
        os.remove(f)


def salva_matriz(file):
    filename = secure_filename(file.filename)
    file.save(os.path.join(app.config["MATRIZ_UPLOADS"],file.filename))
    old_name = os.path.join(app.config["MATRIZ_UPLOADS"],file.filename)
    new_name = os.path.join(app.config["MATRIZ_UPLOADS"],"base.txt")
    os.rename(old_name, new_name)
    url = "app/static/uploads/base.txt"
    df = pd.read_csv(url, sep = ",")
    return len(df.columns)


def salva_modelo(file):
    filename = secure_filename(file.filename)
    file.save(os.path.join(app.config["MATRIZ_UPLOADS"],file.filename))
    old_name = os.path.join(app.config["MATRIZ_UPLOADS"],file.filename)
    new_name = os.path.join(app.config["MATRIZ_UPLOADS"],"model.pck")
    os.rename(old_name, new_name)


def le_csv():
        filename = "app/static/downloads/docsclassificados.csv"
        data = pd.read_csv(filename, header = 0, sep='\t')
        data['ProbGrupo'] = pd.Series(["{0:.2f}%".format(val * 100) for val in data['ProbGrupo']], index = data.index)
        documentos = list(data.values)
        return documentos


def allowed_file_matrix(filename):
    if not "." in filename:
        return False

    ext = filename.rsplit(".", 1)[1]

    if ext.upper() in app.config["ALLOWED_FILE_EXTENSIONS"]:
        return True
    else:
        return False


def allowed_file_model(filename):
    if not "." in filename:
        return False

    ext = filename.rsplit(".", 1)[1]

    if ext.upper() in app.config["ALLOWED_MODEL_EXTENSIONS"]:
        return True
    else:
        return False


def isLogged():
    return ('usuario_logado' in session and session['usuario_logado'] != None)
