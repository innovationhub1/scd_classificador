from app import app
from app import cursor, conn
from app.controllers.funcoes import *
from app.controllers.analise import analise
from flask import request, session, render_template, flash, redirect, send_file


#Realiza o redirecionamento para a página inicial.
@app.route("/index.html", methods = ["GET", "POST"])
def index():
    if(isLogged()):
        session['Uploaded'] = 0
        return render_template('index.html')
    else:
        return redirect("/")


#Realiza o redirecionamento para a página inicial, verificando se o usuário está logado ou não.
@app.route("/", methods = ["GET", "POST"])
def login():
    if(isLogged()):
        session['Login_Alert']=False
        return redirect('/index.html')
    else:
        if(session.get('Login_Alert') == True):
            flash("Por favor, realize o login.")
        session['Login_Alert'] = True
        return render_template("/login.html")


#Realiza o redirecionamento para a página de processamento.
@app.route("/processamento.html", methods = ["GET", "POST"])
def preprocessamento():
    if(isLogged()):
        session['Login_Alert']=False
        return render_template('/processamento.html', num_cols = session.get('Num_Cols'))
    else:
        if(session.get('Login_Alert') == True):
            flash("Por favor, realize o login.")
        session['Login_Alert'] = True
        return render_template("/login.html")


#Método que realiza o upload da base.
@app.route("/upload_matriz", methods = ["GET","POST"])
def upload_matriz():
    if(isLogged()):
        if request.method == "POST":
            if request.files:
                file = request.files['file']

                if file.filename == "":
                    flash("Nenhum arquivo selecionado.")
                    return redirect('/processamento.html')

                if allowed_file_matrix(file.filename):
                    session['Upload_Modelo'] = 0
                    session['Num_Cols'] = salva_matriz(file)
                    session['Uploaded'] = 1
                    flash("Upload realizado com sucesso.")
                    return redirect('/processamento.html')
                else:
                    flash("Extensão de arquivo não permitida.")
                    return redirect('/processamento.html')
    else:
        flash("Por favor, realize o login.")
        return render_template("/login.html")


#Método que realiza o upload de um modelo já criado pelo HDBSCan para reuso.
@app.route("/upload_modelo", methods = ["GET","POST"])
def upload_modelo():
    if(isLogged()):
        if request.method == "POST":
            if request.files:
                file = request.files['model']

                if file.filename == "":
                        flash("Nenhum arquivo selecionado.")
                        session['Upload_Modelo'] = 0
                        return redirect('/processamento.html')

                if allowed_file_model(file.filename):
                    #Chama a função salva_arquivo para salvar o arquivo no computador.
                    salva_modelo(file)
                    session['Upload_Modelo'] = 1
                    flash("Upload do modelo realizado com sucesso.")
                    return redirect('/processamento.html')
                else:
                    flash("Extensão de arquivo não permitida.")
                    return redirect('/processamento.html')
    else:
        flash("Por favor, realize o login.")
        return render_template("/login.html")


#Método que permite realizar o download da base agrupada.
@app.route("/download_groups", methods = ["GET","POST"])
def download_txt():
    return send_file('static/downloads/docsclassificados.csv', mimetype='text/csv', attachment_filename='docsclassificados.csv',as_attachment=True)


#Método que permite realizar o download do modelo criado pelo HDBSCan durante o processamentro
@app.route("/download_model", methods = ["GET","POST"])
def download_model():
    return send_file('static/downloads/model.pck', mimetype='text/csv', attachment_filename='model.pck',as_attachment=True)


#Método de login, verifica se o usuário está cadastrado no banco de dados e inicia a sessão, caso esteja.
@app.route("/autentica_login", methods=['GET','POST'])
def autentica_login():
    #verificar se os campos são válidos
    cursor.execute("""SELECT * FROM Usuarios WHERE usuario = '%s' AND senha = '%s' """ %(request.form['nome'],request.form['senha']))
    if(cursor.fetchone()):  #se logou com sucesso
        flash("Usuário {} foi logado com sucesso".format(request.form['nome']))
        session['usuario_logado'] = request.form['nome']
        return redirect('/index.html')
    else:  #se não logou
        flash("Usuário ou senha incorretos")
        return redirect("/")


#Método que realiza o logout do usuário.
@app.route("/logout", methods=['GET','POST'])
def logout():
    session.clear()
    clean_files()
    return redirect('/')


#Realiza o redirecionamento para a página de registro de novo usuário.
@app.route("/register.html")
def register():
    return render_template('register.html')


#Método que realiza a inserção dos dados do novo usuário no banco de dados.
@app.route("/autentica_cadastro", methods=['GET','POST'])
def autentica_cadastro():
    #verificar se os campos são válidos
    cursor.execute("""INSERT INTO Usuarios(nome, sobrenome, usuario, senha, email) VALUES ('%s', '%s', '%s', '%s', '%s')""" %(request.form['nome'],request.form['sobrenome'],request.form['usuario'],request.form['senha'],request.form['email']))
    conn.commit()
    return redirect('/')


#Redireciona para a recuperação de senha.
@app.route("/forgot-password.html")
def forgot():
    return render_template('forgot-password.html')


#Redireciona para a página de distribuição dos objetos da análise.
@app.route("/distribuicao.html")
def distribuicao():
    if(isLogged()) and session.get('Analysed')== True:
        return render_template('distribuicao.html', vetor_opcoes = session.get('Graphics'))
    elif (isLogged and session.get('Analysed')== None):
        flash("Por favor, inicie o processamento primeiro")
        return redirect("/")
    else:
        flash("Por favor, realize o login.")
        return render_template("/login.html")


#Redireciona para a página de agrupamento dos objetos da análise.
@app.route("/agrupamento.html")
def agrupamento():
    if(isLogged()) and session.get('Analysed')== True:
        return render_template('agrupamento.html', vetor_opcoes = session.get('Graphics'))
    elif (isLogged and session.get('Analysed')== None):
        flash("Por favor, inicie o processamento primeiro")
        return redirect("/")
    else:
        flash("Por favor, realize o login.")
        return render_template("/login.html")


#Redireciona para a página de grupos dos objetos da análise.
@app.route("/grupos.html")
def grupos():
    if(isLogged()) and session.get('Analysed')== True:
        conclusion = session.get('conclusion', None)
        numGrupos = session.get('numGrupos', None)
        cols = session.get('cols', None)
        representativos = session.get('representativos', None)
        caracteristicasMedias = session.get('caracteristicasMedias', None)
        caracteristicasMedianas = session.get('caracteristicasMedianas', None)
        colunas = session.get('colunas', None)
        return render_template('grupos.html',vetor_opcoes = session.get('Graphics'), conclusion = conclusion, numGrupos = numGrupos, cols = cols, representativos = representativos, caracteristicasMedias = caracteristicasMedias, caracteristicasMedianas = caracteristicasMedianas, colunas = colunas)
    elif (isLogged and session.get('Analysed')== None):
        flash("Por favor, inicie o processamento primeiro")
        return redirect("/")
    else:
        mensagem_login()


#Método que chama a análise dos dados.
@app.route("/analise", methods=['GET', 'POST'])
def processamento():
    if(isLogged()) and session.get('Uploaded') == 1:
        vetor_pesos = []
        indice = "p0"
        for i in range(session.get('Num_Cols')):
            indice = indice[:1] + str(i)
            vetor_pesos.append(request.form[indice])

        session['Graphics'] = []
        session['Graphics'].append(request.form.get('distribuicao') != None)
        session['Graphics'].append(request.form.get('abrangencia') != None)
        session['Graphics'].append(request.form.get('dendrograma') != None)
        session['Graphics'].append(request.form.get('clustertree') != None)
        session['Graphics'].append(request.form.get('distribclass') != None)
        session['Graphics'].append(request.form.get('violino') != None)
        session['Salvar_Modelo'] = (request.form.get('salvarmodelo') != None)
        session['MinClusterMetric'] = (request.form.get('MinClusterMetric') != None)

        conclusion, numGrupos, cols, representativos, caracteristicasMedias, caracteristicasMedianas, colunas = analise(vetor_pesos, session.get('Graphics'), session.get('Salvar_Modelo'), session.get('Upload_Modelo'), session.get('MinClusterMetric'))

        session['conclusion'] = conclusion
        session['numGrupos'] = numGrupos
        session['cols'] = cols
        session['representativos'] = representativos
        session['caracteristicasMedias'] = caracteristicasMedias
        session['caracteristicasMedianas'] = caracteristicasMedianas
        session['colunas'] = colunas
        session['Analysed'] = True
        return render_template('distribuicao.html', vetor_opcoes = session.get('Graphics'))
    elif (isLogged and session.get('Uploaded') == 0):
        flash("A base ainda não foi inserida. Faça o Upload da base e tente novamente.")
        return redirect("/processamento.html")
    else:
        flash("Por favor, realize o login.")
        return render_template("/login.html")


#Redireciona para a página da tabela dos dados e de download da base final agrupada.
@app.route("/tables.html")
def tables():
    if(isLogged()) and session.get('Analysed')== True:
        documentos = le_csv()
        return render_template('tables.html', documentos=documentos, salvamodelo = session.get('Salvar_Modelo'))
    elif (isLogged and session.get('Analysed')== None):
        flash("Por favor, inicie o processamento primeiro")
        return redirect("/")
    else:
        flash("Por favor, realize o login.")
        return render_template("/login.html")


#Redireciona páginas desconhecidas para o erro 404.
@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


#Bloqueia o armazenamento de cache para evitar possíveis problemas com imagens de análise anteriores.
@app.after_request
def after_request(response):
    response.headers["Cache-Control"] = "no-cache, no-store, must-revalidate, public, max-age=0"
    response.headers["Expires"] = 0
    response.headers["Pragma"] = "no-cache"
    return response
