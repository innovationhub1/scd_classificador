import os
import six
import hdbscan
import numpy as np
from app import app
import pandas as pd
import seaborn as sns
from joblib import Memory
import matplotlib.cm as cm
from datetime import datetime
import sklearn.datasets as data
import matplotlib.pyplot as plt
from sklearn.manifold import MDS
from app.controllers.scd import *
from sklearn import metrics as me
import sklearn.datasets as dataset
from app.controllers.funcoes import *
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler


def analise(vetor_pesos, vetor_opcoes, salvarmodelo, uploadModelo, MinClusterMetric):

    url = "app/static/uploads/base.txt"
    df = pd.read_csv(url, sep = ",")
    nameColumns = list(df.columns.values)
    nameFiles = df.NomeArq
    df = multiplica_pesos(df, vetor_pesos, nameColumns)
    df = preprocess(df)
    standard_data = standardize(df)

    if(MinClusterMetric == True):
        metric, min_cluster_size = find_best_metric(standard_data, 100, 400)
    else:
        min_cluster_size = 272
        metric = 'braycurtis'

    if(uploadModelo == 0 or uploadModelo == None):
        clusterer = clusterize(standard_data, min_cluster_size, metric, False)
    else:
        clusterer = load_model()

    if(salvarmodelo == True):
        save_model(clusterer)

    std_data_labeled = label_data(standard_data, clusterer, vetor_opcoes)
    conclusion = get_group_silhouette(std_data_labeled, clusterer)

    numGrupos, cols, matriz_concat, da = analise_grupo(df, clusterer, nameFiles)

    salva_imagens(standard_data, std_data_labeled, clusterer, vetor_opcoes, da, cols, numGrupos)

    dt, representativos = mais_representativos(matriz_concat, numGrupos)
    caracteristicasMedias, caracteristicasMedianas, colunas = caracteristicas_grupos(da, numGrupos)

    save_to_csv(dt)

    return conclusion, numGrupos, cols, representativos, caracteristicasMedias, caracteristicasMedianas, colunas
