import pickle
import hdbscan
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.manifold import MDS
from sklearn import metrics as me
from sklearn.preprocessing import StandardScaler


metrics = ['braycurtis',
           'canberra',
           'chebyshev',
           'cityblock',
           'euclidean',
           'hamming']


def binarize_attr(df):
    df['PosAtPa1'] = 0
    df['PosAtPa2'] = 0
    df['PosAtPa3'] = 0
    df['PosAtPa4'] = 0
    df['TipoDoc1'] = 0
    df['TipoDoc2'] = 0
    for i, row in df.iterrows():
        if row['TipoDoc'] == 1:
            df.at[i, 'TipoDoc1'] = 1
        if row['TipoDoc'] == 2:
            df.at[i, 'TipoDoc2'] = 1
        if row['PosAtPa'] == 1:
            df.at[i, 'PosAtPa1'] = 1
        if row['PosAtPa'] == 2:
            df.at[i, 'PosAtPa2'] = 1
        if row['PosAtPa'] == 3:
            df.at[i, 'PosAtPa3'] = 1
        if row['PosAtPa'] == 4:
            df.at[i, 'PosAtPa4'] = 1
    df = df.drop('PosAtPa', axis=1)
    df = df.drop('TipoDoc', axis=1)

    return df


def preprocess(df):
    ndf = binarize_attr(df)
    variables = list(ndf.columns)
    variables.remove('NomeArq')
    return ndf.loc[:, variables]


def get_mds_array(dataframe, dim=2):
    return MDS(n_components=dim).fit_transform(dataframe)


def plot(type, arr, c, s=50, linewidth=0, alpha=0.5, cmap='inferno', exemplars=None):
    plt.figure(figsize=(12, 9), dpi=250)
    plt.scatter(*arr.T, s=50, linewidth=linewidth, c=c, alpha=alpha, cmap=cmap)
    if(type=='distribuicao'):
        plt.savefig("app/static/img/analise/distribuicao_documentos.png",dpi=250,bbox_inches = 'tight',pad_inches = 0.2)
    else:
        plt.savefig("app/static/img/analise/classificado.png",dpi=250,bbox_inches = 'tight',pad_inches = 0.2)
    plt.clf()


def standardize(df):
    x = StandardScaler().fit_transform(df.values)
    data = pd.DataFrame(x)
    return data


def add_labels(df, labels):
    ndf = pd.DataFrame(df)
    ndf['label'] = labels
    return ndf


def drop_outliers(df):
    ndf = df[df['label'].map(lambda x: x == -1) == False]
    return ndf


def find_best_metric(standard_data, x=100, y=400):
    all_metrics = []
    for metric in metrics:
        aux = []
        silhueta, min_cluster = find_min_cluster_size(standard_data, x, y, metric)
        aux = [str(metric), float(silhueta), int(min_cluster)]
        all_metrics.append(aux)

    aux = 0
    indice = None
    for i in range(len(all_metrics)):
        if(aux < all_metrics[i][1]):
            aux = all_metrics[i][1]
            indice = i

    metrica = all_metrics[indice][0]
    min_cluster_size = all_metrics[indice][2]
    return metrica, min_cluster_size


def find_min_cluster_size(standard_data, x=100, y=400, m='braycurtis'):
    """
    Bruteforce all possible incomes given a range. WARNING: Very slow

    Parameters
    ----------
    standard_data
    x
    y
    m

    Returns
    -------
    Best results for silhouette and minimum cluster size

    """
    all_classifications = {}
    cluster_size = []
    for i in range(x, y):
        test_data = standard_data
        clusterer = hdbscan.HDBSCAN(min_cluster_size=i, gen_min_span_tree=True, metric=m,
                                    prediction_data=False)
        clusterer.fit(standard_data)
        std_data_labeled = add_labels(test_data, clusterer.labels_)
        std_data_labeled = drop_outliers(std_data_labeled)
        x = clusterer.labels_[clusterer.labels_ != -1]
        try:
            a = me.silhouette_score(std_data_labeled.iloc[:, :-1], x, metric=m)
        except:
            pass
        if a != 1 and len(pd.Series(clusterer.labels_).value_counts()) > 2:
            cluster_size.append(a)
            all_classifications[a] = (i, m, pd.Series(clusterer.labels_).value_counts())
    min_cluster_size, metric, _ = all_classifications[max(all_classifications)]
    print("Max sillhouete score", max(all_classifications), "\nmetric", metric, "\nmin_cluster:", min_cluster_size)
    return max(all_classifications), min_cluster_size


def get_group_silhouette(std_data_labeled, clusterer):
    """

    Parameters
    ----------
    std_data_labeled
    clusterer

    Returns
    -------

    Resultados finais da clusterização

    """

    std_data_labeled['silhouette'] = me.silhouette_samples(std_data_labeled.iloc[:, :-1], std_data_labeled['label'],
                                                           metric='braycurtis')
    conclusion = []
    for g in range(0, len(pd.Series(clusterer.labels_).value_counts()) - 1):
        subgroup = std_data_labeled[std_data_labeled['label'] == g]
        conclusion.append((subgroup['silhouette'].mean(), subgroup['silhouette'].std(),
                           (len(subgroup) / len(clusterer.labels_)) * 100, g))
    conclusion.sort()
    return conclusion


def get_mds(std_data_labeled, color, tipo):
    mds = get_mds_array(std_data_labeled.iloc[:, :-1])
    plot(tipo, mds, std_data_labeled['label'], cmap=color)


def clusterize(standard_data, min_cluster_size=272, metric='braycurtis', prediction_data=False):
    clusterer = hdbscan.HDBSCAN(min_cluster_size=min_cluster_size, gen_min_span_tree=True, min_samples=1, metric=metric,
                                prediction_data=prediction_data)
    clusterer.fit(standard_data)

    return clusterer


def label_data(standard_data, clusterer, vetor_opcoes):
    std_data_labeled = add_labels(standard_data, clusterer.labels_)
    std_data_labeled = drop_outliers(std_data_labeled)

    return std_data_labeled


def save_model(clusterer, path='app/static/downloads/model.pck'):
    with open(path, 'wb') as handle:
        pickle.dump(clusterer, handle, protocol=pickle.HIGHEST_PROTOCOL)


def load_model(path='app/static/uploads/model.pck'):
    with open(path, 'rb') as handle:
        clusterer = pickle.load(handle)

    return clusterer


def predict_new_data(clusterer, standard_data):
    """

    Parameters
    ----------
    clusterer
    standard_data

    Returns
    -------
    Predicted data without fitting the model

    """
    labels, _ = hdbscan.approximate_predict(clusterer, standard_data)
    return labels
