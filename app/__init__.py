from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
import psycopg2

app = Flask(__name__)
app.secret_key='innovationDB'
#conn_string = "host=localhost dbname=innovationdb user=innovation password = innovation"
conn_string = "host=localhost dbname=innovationdb user=lcapellaro password = zipperson"
conn = psycopg2.connect(conn_string)
cursor = conn.cursor()

from app.controllers import default
