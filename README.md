---------------------------------------------------------Como Usar----------------------------------------------------------------

Sistema Operacional Utilizado - Kubuntu 19.04

Tutorial para rodar o server:

Após realizar o download deste repositório do git, instale uma IDE de sua preferência. Para instalar a IDE Atom, abra o terminal e digite:

$ snap install atom --classic

Feito isto, instale o ambiente virtual virtuaenv:

$ pip3 install virtualenv
$ virtualenv -p python3 venv

Após isso, para executar o ambiente virtual, vá até a pasta Raiz do diretório clonado e digite:

$ . venv/bin/activate

Perceba que antes do seu nome de usuário no termina, aparecerá a palavra (venv). Agora, para iniciar o servidor, digite:

$ python3 run.py runserver

Feito isto, para acessar o website digite em seu navegador o endereço: 127.0.0.1:5000.
